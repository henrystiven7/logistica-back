package com.hsmontano.logistica.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hsmontano.logistica.domains.entities.Almacenamiento;

public interface AlmacenamientoRepository extends JpaRepository<Almacenamiento, Long> {

}
