package com.hsmontano.logistica.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hsmontano.logistica.domains.entities.Ruta;

public interface RutaRepository extends JpaRepository<Ruta, Long>{

}
