package com.hsmontano.logistica.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hsmontano.logistica.domains.entities.PlanEntrega;

public interface PlanEntregaRepository extends JpaRepository<PlanEntrega, Long>{

}
