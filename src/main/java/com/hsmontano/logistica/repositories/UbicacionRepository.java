package com.hsmontano.logistica.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hsmontano.logistica.domains.entities.Ubicacion;

public interface UbicacionRepository extends JpaRepository<Ubicacion, Long>{

}
