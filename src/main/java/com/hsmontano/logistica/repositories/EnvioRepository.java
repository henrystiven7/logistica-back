package com.hsmontano.logistica.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hsmontano.logistica.domains.entities.Envio;

public interface EnvioRepository extends JpaRepository<Envio, Long> {

}
