package com.hsmontano.logistica.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hsmontano.logistica.domains.entities.TipoProducto;

public interface TipoProductoRepository extends JpaRepository<TipoProducto, Long>{

}
