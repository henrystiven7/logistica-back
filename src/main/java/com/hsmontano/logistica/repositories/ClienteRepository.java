package com.hsmontano.logistica.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hsmontano.logistica.domains.entities.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

}
