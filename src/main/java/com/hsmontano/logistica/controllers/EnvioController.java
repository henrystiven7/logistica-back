package com.hsmontano.logistica.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hsmontano.logistica.domains.entities.Envio;
import com.hsmontano.logistica.services.EnvioService;

@RestController
@RequestMapping("/api")
public class EnvioController {

	@Autowired
	private EnvioService envioService;
	
	@GetMapping("/envios/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Envio show(@PathVariable Long id) {
		return envioService.findById(id);
	}
}
