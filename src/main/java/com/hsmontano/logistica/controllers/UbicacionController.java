package com.hsmontano.logistica.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsmontano.logistica.domains.entities.Ubicacion;
import com.hsmontano.logistica.services.UbicacionService;

@RestController
@RequestMapping("/api")
public class UbicacionController {

	@Autowired
	private UbicacionService ubicacionService;
	
	@GetMapping("/ubicaciones/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		Ubicacion ubicacion = null;
		Map<String, Object> response = new HashMap<>();

		try {

			ubicacion = ubicacionService.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (ubicacion == null) {
			response.put("mensaje", "La ubicacion con ID: ".concat(id.toString().concat("No está registrada")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Ubicacion>(ubicacion, HttpStatus.OK);
	}
	
	@PostMapping("/ubicaciones")
	public ResponseEntity<?> create(@Valid @RequestBody Ubicacion ubicacion) {

		Ubicacion ubicacionNew = null;
		Map<String, Object> response = new HashMap<>();

		try {

			ubicacionNew = ubicacionService.save(ubicacion);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al realizar el registro en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		response.put("mensaje", "La ubicacion ha sido creada con exito!");
		response.put("ubicacion", ubicacionNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("/ubicaciones/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Ubicacion ubicacion, @PathVariable Long id) {

		Ubicacion ubicacionCurrent = ubicacionService.findById(id);
		Ubicacion ubicacionUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if (ubicacionCurrent == null) {

			response.put("mensaje", "La ubicación actual no se pudo actualizar, la ubicacion con ID: "
					.concat(id.toString().concat("No se encuentra registrado en el sistema")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			ubicacionCurrent.setCarrera(ubicacion.getCarrera());
			ubicacionCurrent.setCalle(ubicacion.getCalle());
			ubicacionCurrent.setCiudad(ubicacion.getCiudad());
			ubicacionCurrent.setEstado(ubicacion.getEstado());
			ubicacionCurrent.setNumeroCasa(ubicacion.getNumeroCasa());
			ubicacionCurrent.setPais(ubicacion.getPais());
			ubicacionCurrent.setCodigoPostal(ubicacion.getCodigoPostal());
			
			ubicacionUpdated = ubicacionService.save(ubicacionCurrent);
		} catch (DataAccessException e) {

			response.put("mensaje", "La ubicación no se pudo actualizar ocurrió un error");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La ubicacion ha sido actualizada!");
		response.put("ubicacion", ubicacionUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/ubicaciones/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();

		try {

			ubicacionService.delete(id);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al eliminar la ubicación del sistema");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La ubicación se ha eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
