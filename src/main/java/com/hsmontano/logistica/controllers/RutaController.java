package com.hsmontano.logistica.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsmontano.logistica.domains.entities.Ruta;
import com.hsmontano.logistica.services.RutaService;

@RestController
@RequestMapping("/api")
public class RutaController {

	@Autowired
	private RutaService rutaService;
	
	@GetMapping("/rutas/pages/{page}")
	public Page<Ruta> findAll(@PathVariable Integer page){
		Pageable pageable = PageRequest.of(page, 5);
		return rutaService.findAll(pageable);
	}
	
	@GetMapping("/rutas/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		Ruta ruta = null;
		Map<String, Object> response = new HashMap<>();

		try {

			ruta = rutaService.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (ruta == null) {
			response.put("mensaje", "La ruta con ID: ".concat(id.toString().concat("No está registrado")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Ruta>(ruta, HttpStatus.OK);
	}
	
	@PostMapping("/rutas")
	public ResponseEntity<?> create(@Valid @RequestBody Ruta ruta) {

		Ruta rutaNew = null;
		Map<String, Object> response = new HashMap<>();

		try {

			rutaNew = rutaService.save(ruta);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al realizar el registro en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		response.put("mensaje", "La ruta ha sido creado con exito!");
		response.put("ruta", rutaNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("/rutas/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Ruta ruta, @PathVariable Long id) {

		Ruta rutaCurrent = rutaService.findById(id);
		Ruta rutaUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if (rutaCurrent == null) {

			response.put("mensaje", "La ruta actual no se pudo actualizar, la ruta con ID: "
					.concat(id.toString().concat("No se encuentra registrado en el sistema")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			rutaCurrent.setOrigenAlmtoId(ruta.getOrigenAlmtoId());
			rutaCurrent.setDestinoAlmtoId(ruta.getDestinoAlmtoId());
			rutaCurrent.setFechaRegistro(new Date());
			rutaCurrent.setRefTransp(ruta.getRefTransp());
			
			rutaUpdated = rutaService.save(rutaCurrent);
			
		} catch (DataAccessException e) {

			response.put("mensaje", "La ruta no se pudo actualizar ocurrió un error");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La ruta ha sido actualizado!");
		response.put("ruta", rutaUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/rutas/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();

		try {

			rutaService.delete(id);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al eliminar la ruta del sistema");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La ruta se ha eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
