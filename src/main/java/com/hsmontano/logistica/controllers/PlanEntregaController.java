package com.hsmontano.logistica.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsmontano.logistica.domains.entities.Almacenamiento;
import com.hsmontano.logistica.domains.entities.PlanEntrega;
import com.hsmontano.logistica.services.PlanEntregaService;

@RestController
@RequestMapping("/api")
public class PlanEntregaController {

	@Autowired
	private PlanEntregaService entregaService;
	
	@GetMapping("/entregas/pages/{page}")
	public Page<PlanEntrega> findAll(@PathVariable Integer page){
		Pageable pageable = PageRequest.of(page, 5);
		return entregaService.findAll(pageable);
	}
	
	@GetMapping("/entregas/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		PlanEntrega entrega  = null;
		Map<String, Object> response = new HashMap<>();

		try {

			entrega = entregaService.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (entrega == null) {
			response.put("mensaje", "La entrega con ID: ".concat(id.toString().concat("No está registrado")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<PlanEntrega>(entrega, HttpStatus.OK);
	}
	
	@PostMapping("/entregas")
	public ResponseEntity<?> create(@Valid @RequestBody PlanEntrega entrega) {

		PlanEntrega entregaNew = null;
		Map<String, Object> response = new HashMap<>();

		try {

			entregaNew = entregaService.save(entrega);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al realizar el registro en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		response.put("mensaje", "La entrega ha sido creado con exito!");
		response.put("entrega", entregaNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/entregas/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody PlanEntrega entrega, @PathVariable Long id) {

		PlanEntrega entregaCurrent = entregaService.findById(id);
		PlanEntrega entregaUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if (entregaCurrent == null) {

			response.put("mensaje", "La entrega actual no se pudo actualizar, la entrega con ID: "
					.concat(id.toString().concat("No se encuentra registrado en el sistema")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			entregaCurrent.setEnvio(entrega.getEnvio());
			
			entregaUpdated = entregaService.save(entregaCurrent);
		} catch (DataAccessException e) {

			response.put("mensaje", "La entrega no se pudo actualizar ocurrió un error");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La entrega ha sido actualizada!");
		response.put("entrega", entregaUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
