package com.hsmontano.logistica.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsmontano.logistica.domains.entities.TipoProducto;
import com.hsmontano.logistica.services.TipoProductoService;

@RestController
@RequestMapping("/api/tipo")
public class TipoProductoController {
	
	@Autowired
	public TipoProductoService tipoProductoService;
	
	@GetMapping("/productos/pages/{page}")
	public Page<TipoProducto> findAll(@PathVariable Integer page){		
		Pageable pageable = PageRequest.of(page, 5);
		return tipoProductoService.findAll(pageable);
	}
	
	@GetMapping("/productos/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		TipoProducto tipoProducto = null;
		Map<String, Object> response = new HashMap<>();

		try {

			tipoProducto = tipoProductoService.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (tipoProducto == null) {
			response.put("mensaje", "El tipo producto con ID: ".concat(id.toString().concat("No está registrado")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<TipoProducto>(tipoProducto, HttpStatus.OK);
	}
	
	@PostMapping("/productos")
	public ResponseEntity<?> create(@Valid @RequestBody TipoProducto tipoProducto) {

		TipoProducto tipoProductoNew = null;
		Map<String, Object> response = new HashMap<>();

		try {

			tipoProductoNew = tipoProductoService.save(tipoProducto);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al realizar el registro en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		response.put("mensaje", "El producto ha sido creado con exito!");
		response.put("tipo producto", tipoProductoNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("recursos/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody TipoProducto tipoProducto, @PathVariable Long id) {

		TipoProducto tipoProductoCurrent = tipoProductoService.findById(id);
		TipoProducto tipoProductoUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if (tipoProductoCurrent == null) {

			response.put("mensaje", "El tipo producto actual no se pudo actualizar, el recurso con ID: "
					.concat(id.toString().concat("No se encuentra registrado en el sistema")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			tipoProductoCurrent.setNombre(tipoProducto.getNombre());
			
			tipoProductoUpdated = tipoProductoService.save(tipoProductoCurrent);
		} catch (DataAccessException e) {

			response.put("mensaje", "El tipo producto no se pudo actualizar ocurrió un error");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El producto ha sido actualizado!");
		response.put("producto", tipoProductoUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("recursos/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();

		try {

			tipoProductoService.delete(id);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al eliminar el tipo producto del sistema");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El tipo producto se ha eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
