package com.hsmontano.logistica.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsmontano.logistica.domains.entities.Almacenamiento;
import com.hsmontano.logistica.services.AlmacenamientoService;

@RestController
@RequestMapping("/api")
public class AlmacenamientoController {

	@Autowired
	private AlmacenamientoService almacenamientoService;
	
	@GetMapping("/almacenamientos/pages/{page}")
	public Page<Almacenamiento> findAll(@PathVariable Integer page){
		Pageable pageable = PageRequest.of(page, 5);
		return almacenamientoService.findAll(pageable);
	}
	
	@GetMapping("/almacenamientos/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		Almacenamiento almacenamiento = null;
		Map<String, Object> response = new HashMap<>();

		try {

			almacenamiento = almacenamientoService.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (almacenamiento == null) {
			response.put("mensaje", "La bodega o puerto con ID: ".concat(id.toString().concat("No está registrado")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Almacenamiento>(almacenamiento, HttpStatus.OK);
	}
	
	@PostMapping("/almacenamientos")
	public ResponseEntity<?> create(@Valid @RequestBody Almacenamiento almacenamiento) {

		Almacenamiento almacenamientoNew = null;
		Map<String, Object> response = new HashMap<>();

		try {

			almacenamientoNew = almacenamientoService.save(almacenamiento);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al realizar el registro en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		response.put("mensaje", "El almacenamiento ha sido creado con exito!");
		response.put("almacenamiento", almacenamientoNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/almacenamientos/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Almacenamiento almacenamiento, @PathVariable Long id) {

		Almacenamiento almacenamientoCurrent = almacenamientoService.findById(id);
		Almacenamiento almacenamientoUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if (almacenamientoCurrent == null) {

			response.put("mensaje", "La bodega o puerto actual no se pudo actualizar, el almacenamiento con ID: "
					.concat(id.toString().concat("No se encuentra registrado en el sistema")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			almacenamientoCurrent.setNombre(almacenamiento.getNombre());
			almacenamientoCurrent.setUbicacion(almacenamiento.getUbicacion());
			
			almacenamientoUpdated = almacenamientoService.save(almacenamientoCurrent);
		} catch (DataAccessException e) {

			response.put("mensaje", "El almacenamiento no se pudo actualizar ocurrió un error");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El almacenamiento ha sido actualizado!");
		response.put("almacenamiento", almacenamientoUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/almacenamientos/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();

		try {

			almacenamientoService.delete(id);

		} catch (DataAccessException e) {

			response.put("mensaje", "Error al eliminar el almacenamiento del sistema");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El almacenamiento se ha eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
