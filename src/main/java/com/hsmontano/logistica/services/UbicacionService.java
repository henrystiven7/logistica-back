package com.hsmontano.logistica.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hsmontano.logistica.domains.entities.Ubicacion;

public interface UbicacionService {

	public Page<Ubicacion> findAll(Pageable pageable);
	
	public Ubicacion findById(Long id);
	
	public Ubicacion save(Ubicacion ubicacion);
	
	public void delete(Long id);
}
