package com.hsmontano.logistica.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hsmontano.logistica.domains.entities.PlanEntrega;

public interface PlanEntregaService {

	public Page<PlanEntrega> findAll(Pageable pageable);
	
	public PlanEntrega findById(Long id);
	
	public PlanEntrega save(PlanEntrega entrega);
	
	public void delete(Long id);
}
