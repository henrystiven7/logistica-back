package com.hsmontano.logistica.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hsmontano.logistica.domains.entities.TipoProducto;

public interface TipoProductoService {

	public Page<TipoProducto> findAll(Pageable pageable);
	
	public TipoProducto findById(Long id);

	public TipoProducto save(TipoProducto tipoProducto);

	public void delete(Long id);
}
