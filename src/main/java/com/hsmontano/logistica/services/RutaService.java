package com.hsmontano.logistica.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hsmontano.logistica.domains.entities.Ruta;

public interface RutaService {
	
	public Page<Ruta> findAll(Pageable pageable);
	
	public Ruta findById(Long id);
	
	public Ruta save(Ruta ruta);
	
	public void delete(Long id);
}
