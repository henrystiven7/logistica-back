package com.hsmontano.logistica.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hsmontano.logistica.domains.entities.Almacenamiento;

public interface AlmacenamientoService {

	public Page<Almacenamiento> findAll(Pageable pageable);
	
	public Almacenamiento findById(Long id);
	
	public Almacenamiento save(Almacenamiento almacenamiento);
	
	public void delete(Long id);
}
