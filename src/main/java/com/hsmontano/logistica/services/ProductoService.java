package com.hsmontano.logistica.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hsmontano.logistica.domains.entities.Producto;

public interface ProductoService {

	public Page<Producto> findAll(Pageable pageable);
	
	public Producto findById(Long id);

	public Producto save(Producto producto);

	public void delete(Long id);

}
