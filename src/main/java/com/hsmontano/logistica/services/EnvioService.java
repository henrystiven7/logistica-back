package com.hsmontano.logistica.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hsmontano.logistica.domains.entities.Envio;

public interface EnvioService {

	public Page<Envio> findAll(Pageable pageable);
	
	public Envio findById(Long id);
	
	public Envio save(Envio envio);
	
	public void delete(Long id);
}
