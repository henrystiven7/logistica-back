package com.hsmontano.logistica.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hsmontano.logistica.domains.entities.Producto;
import com.hsmontano.logistica.repositories.ProductoRepository;
import com.hsmontano.logistica.services.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	private ProductoRepository productoService;
	
	@Override
	@Transactional(readOnly = true)
	public Page<Producto> findAll(Pageable pageable) {
	
		return productoService.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Producto findById(Long id) {
		return productoService.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Producto save(Producto producto) {
		return productoService.save(producto);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		productoService.deleteById(id);
	}

}
