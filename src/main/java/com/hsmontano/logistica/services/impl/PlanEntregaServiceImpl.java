package com.hsmontano.logistica.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hsmontano.logistica.domains.entities.PlanEntrega;
import com.hsmontano.logistica.repositories.PlanEntregaRepository;
import com.hsmontano.logistica.services.PlanEntregaService;

@Service
public class PlanEntregaServiceImpl implements PlanEntregaService {

	@Autowired
	private PlanEntregaRepository planEntregaRepository;
	
	@Override
	@Transactional(readOnly = true)
	public Page<PlanEntrega> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return planEntregaRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public PlanEntrega findById(Long id) {
		// TODO Auto-generated method stub
		return planEntregaRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public PlanEntrega save(PlanEntrega entrega) {
		// TODO Auto-generated method stub
		return planEntregaRepository.save(entrega);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		planEntregaRepository.deleteById(id);
	}

}
