package com.hsmontano.logistica.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hsmontano.logistica.domains.entities.Envio;
import com.hsmontano.logistica.repositories.EnvioRepository;
import com.hsmontano.logistica.services.EnvioService;

@Service
public class EnvioServiceImpl implements EnvioService {

	@Autowired
	private EnvioRepository envioRepository;
	
	@Override
	@Transactional(readOnly = true)
	public Page<Envio> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return envioRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Envio findById(Long id) {
		// TODO Auto-generated method stub
		return envioRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Envio save(Envio envio) {
		// TODO Auto-generated method stub
		return envioRepository.save(envio);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		envioRepository.deleteById(id);
	}

}
