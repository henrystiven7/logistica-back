package com.hsmontano.logistica.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hsmontano.logistica.domains.entities.Ubicacion;
import com.hsmontano.logistica.repositories.UbicacionRepository;
import com.hsmontano.logistica.services.UbicacionService;

@Service
public class UbicacionServiceImpl implements UbicacionService{

	@Autowired
	private UbicacionRepository ubicacionRepository;
	
	@Override
	@Transactional(readOnly = true)
	public Page<Ubicacion> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return ubicacionRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Ubicacion findById(Long id) {
		// TODO Auto-generated method stub
		return ubicacionRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Ubicacion save(Ubicacion ubicacion) {
		// TODO Auto-generated method stub
		return ubicacionRepository.save(ubicacion);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		ubicacionRepository.deleteById(id);
	}

}
