package com.hsmontano.logistica.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hsmontano.logistica.domains.entities.TipoProducto;
import com.hsmontano.logistica.repositories.TipoProductoRepository;
import com.hsmontano.logistica.services.TipoProductoService;

@Service
public class TipoProductoServiceImpl implements TipoProductoService {

	@Autowired
	private TipoProductoRepository tipoProductoRepository;
	
	@Override
	@Transactional(readOnly = true)
	public Page<TipoProducto> findAll(Pageable pageable) {
		
		return tipoProductoRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public TipoProducto findById(Long id) {
		return tipoProductoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TipoProducto save(TipoProducto tipoProducto) {
		
		return tipoProductoRepository.save(tipoProducto);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		tipoProductoRepository.deleteById(id);
	}

}
