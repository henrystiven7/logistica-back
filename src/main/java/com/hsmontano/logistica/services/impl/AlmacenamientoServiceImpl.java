package com.hsmontano.logistica.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hsmontano.logistica.domains.entities.Almacenamiento;
import com.hsmontano.logistica.repositories.AlmacenamientoRepository;
import com.hsmontano.logistica.services.AlmacenamientoService;

@Service
public class AlmacenamientoServiceImpl implements AlmacenamientoService {

	@Autowired
	private AlmacenamientoRepository almacenamientoRepository;
	
	@Override
	@Transactional(readOnly = true)
	public Page<Almacenamiento> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return almacenamientoRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Almacenamiento findById(Long id) {
		// TODO Auto-generated method stub
		return almacenamientoRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Almacenamiento save(Almacenamiento almacenamiento) {
		// TODO Auto-generated method stub
		return almacenamientoRepository.save(almacenamiento);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		almacenamientoRepository.deleteById(id);
	}

}
