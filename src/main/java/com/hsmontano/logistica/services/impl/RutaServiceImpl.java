package com.hsmontano.logistica.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hsmontano.logistica.domains.entities.Ruta;
import com.hsmontano.logistica.repositories.RutaRepository;
import com.hsmontano.logistica.services.RutaService;

@Service
public class RutaServiceImpl implements RutaService {

	@Autowired
	private RutaRepository rutaRepository;
	
	@Override
	@Transactional(readOnly = true)
	public Page<Ruta> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return rutaRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Ruta findById(Long id) {
		// TODO Auto-generated method stub
		return rutaRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Ruta save(Ruta ruta) {
		// TODO Auto-generated method stub
		return rutaRepository.save(ruta);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		rutaRepository.deleteById(id);
	}

}
