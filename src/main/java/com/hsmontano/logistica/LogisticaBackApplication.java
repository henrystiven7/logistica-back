package com.hsmontano.logistica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogisticaBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogisticaBackApplication.class, args);
	}

}
