package com.hsmontano.logistica.domains.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tbl_rutas")
public class Ruta implements Serializable {

	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;

	@NotNull(message = "no puede ser vacio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "origen_almto_id", referencedColumnName = "id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Almacenamiento origenAlmtoId;

	@NotNull(message = "no puede ser vacio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destino_almto_id", referencedColumnName = "id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Almacenamiento destinoAlmtoId;

	@Enumerated(value = EnumType.STRING)
	@Column(name = "transporte_id")
	private Transporte transporte;

	@NotNull(message = "no puede ser vacio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plan_entrega_id", referencedColumnName = "id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private PlanEntrega planEntrega;

	@Column(name = "ref_transp")
	private String refTransp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_registro")
	private Date fechaRegistro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Almacenamiento getOrigenAlmtoId() {
		return origenAlmtoId;
	}

	public void setOrigenAlmtoId(Almacenamiento origenAlmtoId) {
		this.origenAlmtoId = origenAlmtoId;
	}

	public Almacenamiento getDestinoAlmtoId() {
		return destinoAlmtoId;
	}

	public void setDestinoAlmtoId(Almacenamiento destinoAlmtoId) {
		this.destinoAlmtoId = destinoAlmtoId;
	}

	public Transporte getTransporte() {
		return transporte;
	}

	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}

	public PlanEntrega getPlanEntrega() {
		return planEntrega;
	}

	public void setPlanEntrega(PlanEntrega planEntrega) {
		this.planEntrega = planEntrega;
	}

	public String getRefTransp() {
		return refTransp;
	}

	public void setRefTransp(String refTransp) {
		this.refTransp = refTransp;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

}
