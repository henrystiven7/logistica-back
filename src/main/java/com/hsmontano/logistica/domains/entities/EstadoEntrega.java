package com.hsmontano.logistica.domains.entities;

public enum EstadoEntrega {
	NO_ENTREGADO, ENTREGADO
}
