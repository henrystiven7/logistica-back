package com.hsmontano.logistica.domains.dtos;

import java.io.Serializable;

public class TipoProductoDto implements Serializable {

	private static final long serialVersionUID = 5768578186755480520L;
	private Long id;
	private String nombre;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
